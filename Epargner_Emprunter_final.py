# -*- coding: utf-8 -*-

import matplotlib
import tkinter as tk

def emprunt_immo():
    montant = 250000
    temps = 20
    taux = 0.05
    #calcul taux mensuel
    taux_mensuel = taux/12
    #calcul nombre de mensualités
    nb_mens = temps*12
    #calcul de la mensualité
    mensualite = taux_mensuel * montant * (((1+taux_mensuel)**nb_mens)/(((1+taux_mensuel)**nb_mens)-1))
    #somme des mensualités
    somme_mens = mensualite
    #copies du montant de base afin de le conserver
    montant2 = montant
    montant_neg = montant
    remb = []
    remb2 = []
    
    final_str = 'les mensualités à régler sont de', round(mensualite, 2), '€.\n'

    for i in range(nb_mens):
        montant2 = montant2 * (1+taux_mensuel) - mensualite
        somme_mens += mensualite
        remb.append(montant2)
        if(i == 12):
            total_1an = round(somme_mens, 2)
        
    final_str += 'vous avez remboursé au total', round(somme_mens,2), '€, soit', round(somme_mens/montant, 3), 'fois plus.\n'
    final_str +='au bout d\'un an, vous avez payé au total', total_1an, '€, cependant vous n\'avez rembousé que', round(250000-remb[11], 2), '€, soit', round(((250000-remb[11])/total_1an)*100, 2), '% de l\'argent reversé remboursant réelement le prêt.\n'
    mens_max = input('Rentrez ici un montant maximal que vous êtes prêt à payer par mensualité, nous allons vous calculer le prêt avec le moins d\'intérêts possible, donc le plus court possible, au même teux qu\'auparavant.')
    for l in range(1, 1200):
        mens2 = taux_mensuel * montant * (((1+taux_mensuel)**(l))/(((1+taux_mensuel)**l)-1))
        if(round(mens2, 2) <= round(float(mens_max), 2)):
            nb_mens2 = l
            break
    mens2 = taux_mensuel * montant * (((1+taux_mensuel)**(nb_mens2))/(((1+taux_mensuel)**nb_mens2)-1))
    somme_mens2 = mens2
    for k in range(nb_mens2):
        montant_neg = montant_neg * (1+taux_mensuel) - mens2
        somme_mens2 += mens2
        remb2.append(montant_neg)
        if(k == 12):
            total_1an = round(somme_mens2, 2)
        if(montant_neg == 0):
            print(k)
    
    final_str +='si vous renégociez ce contrat après',nb_mens2, 'mois soit', round(nb_mens2/12, 2), 'ans, vous paierez des mensualités de', round(mens2, 2), '€ pour avoir déboursé au final', round(somme_mens2, 2), '€, soit', round(somme_mens2/montant, 3), 'fois plus.\n'
    final_str +='de plus, après un an, vous aurez déboursé au total', total_1an, '€ mais vous aurez rembousé', round(250000-remb2[11], 2), '€, soit', round(((250000-remb2[11])/total_1an)*100, 2), '% de l\'argent reversé remboursant réelement le prêt.\n'
    
    
    #init interface
    frame_exo = tk.Message(root, text=final_str)
    frame_exo.pack()
    
    matplotlib.pyplot.plot(remb)
    matplotlib.pyplot.plot(remb2)
    matplotlib.pyplot.show() 
  
#-------------------------------------------------------------------------------
#--------------------------------fin exo 3--------------------------------------
#-------------------------------------------------------------------------------


def plan_retraite():
    apport_annuel = 2000
    temps = 40
    taux = 0.05
    montant2 = apport_annuel
    for i in range (temps):
        montant2 = montant2*(1+taux) + apport_annuel
    
    print('le plan retraite dispose à la fin des', temps, 'années de cotisation d\'un capital de', round(montant2, 2), 'euros.')    
    print('sachant qu\'il a déposé au total', temps*apport_annuel, '€ à un taux de', taux*100, '%, soit', round(montant2/(temps*apport_annuel), 2), 'fois plus.')

#-------------------------------------------------------------------------------
#--------------------------------fin exo 4--------------------------------------
#-------------------------------------------------------------------------------
    
def actualisation():
    somme=int(input('Quelle somme voulez-vous actualiser ?'))
    temps=int(input('Sur combien de temps voulez-vous actualiser ?'))
    taux=float(input('A quel taux l\'actualisation doit être effectuée ?'))
    taux=taux/100
    placement = somme/(1+taux)**temps
    print('il faudra placer', round(placement,2), '€ pour obtenir', somme, '€ à', taux*100, '% en', temps, 'ans.')
    
#-------------------------------------------------------------------------------
#--------------------------------fin exo 5--------------------------------------
#-------------------------------------------------------------------------------
#---------------------Définission inferface graphique---------------------------
#-------------------------------------------------------------------------------   
   
root = tk.Tk()  
frame = tk.Frame(root)
frame.pack()
button = tk.Button(frame, text="Exercice 3", command=emprunt_immo)
button.pack(side=tk.LEFT)

button2 = tk.Button(frame, text="Exercice 4", command=plan_retraite)
button2.pack(side=tk.TOP)

button3 = tk.Button(frame, text="Exercice 5", command=actualisation)
button3.pack(side=tk.RIGHT)

root.mainloop()